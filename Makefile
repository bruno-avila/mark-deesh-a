CC=gcc
LDFLAGS := -Llib/ -rdynamic
LDLIBS := -lraylib -lm -lglfw
PROJ=mark-a
CPPFLAGS := -Iinclude/
SRC := $(wildcard src/*.c)
OBJ := $(SRC:src/%.c=obj/%.o)
CFLAGS := -Wall -std=c99 -MD -mno-needed -g
BUILD_MODE ?= DEBUG

ifeq ($(BUILD_MODE), DEBUG)
	CFLAGS += -D_DEBUG -g
endif

mark-a: $(OBJ)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

obj/%.o: src/%.c | obj
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

obj:
	mkdir -p $@

clean:
	rm obj/*.o
	rm obj/*.d
	rm -r obj
	rm $(PROJ)

-include $(OBJ:.o=.d)
