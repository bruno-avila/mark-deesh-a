#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "raylib.h"

#include "action.h"
#include "actor.h"
#include "camera.h"
#include "collision.h"
#include "editor.h"
#include "entity.h"
#include "func.h"
#include "game.h"
#include "info.h"
#include "input.h"
#include "punctum.h"
#include "solid.h"
#include "system.h"
#include "world.h"

#endif
