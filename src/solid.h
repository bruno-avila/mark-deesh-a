#ifndef SOLID_H
#define SOLID_H
#include "common.h"

#define SOLIDS_LIMIT 65535

typedef struct Solid{
  Rectangle rect;
  Color cor;
  uint32_t state;
}Solid;

extern struct Solid solids[];

struct Solid *create_solid();
void parse_solids(uint32_t *solids_buffer, uint32_t solids_size);

#endif
