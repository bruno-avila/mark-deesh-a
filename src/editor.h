#ifndef EDITOR_H
#define EDITOR_H
#include "common.h"

#define EDITOR_GRID_WIDTH 256
#define EDITOR_GRID_HEIGHT 256

enum editor_state{
  EDST_START,
  EDST_LOOP,

  EDST_COUNT
};

enum layer_type{
  LATY_SOLID,
  LATY_ACTOR,
  LATY_BACKG,
  LATY_MIDDLEG,
  LATY_FOREG,

  LATY_COUNT
};

extern uint8_t layer_type;
extern uint8_t editor_state;
extern uint32_t editor_grid_size;
extern uint8_t *editor_solid_buffer;
extern uint8_t *editor_actor_buffer;
extern uint8_t *editor_backg_buffer;
extern uint8_t *editor_middleg_buffer;
extern uint8_t *editor_foreg_buffer;
extern uint8_t *editor_current_buffer;
extern uint8_t tile_option;

void loop_editor();
void editor_save_file();
void editor_write_file(uint8_t *writing_buffer, char filename[30]);
void editor_load_file();
void editor_read_file(uint8_t *reading_buffer, char filename[30]);
void change_chosen_tile(uint8_t val);
void change_editing_layer();
void update_editor_input();
void draw_editor();

#endif
