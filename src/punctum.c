#include "common.h"

Punctum puncta[256] = {0};

Punctum *create_punctum(Punctum *punctum){
  for(int i = 0; i < 256; i++){
    if(puncta[i].state == 0){
      puncta[i].master = punctum;
      puncta[i].state = 1;
      return &puncta[i];
    }
  }
  printf("Failed to create Punctum!\n");
  return NULL;
}

void delete_punctum(Punctum *p){
  if(p->slave != NULL){
    delete_punctum(p->slave);
  }
  *p = (Punctum){0};
}

void update_puncta(){
  for(int i = 0; i < 256; i++){
    if(puncta[i].state == 1 && puncta[i].master == NULL){
      puncta[i].position.x += puncta[i].velocity.x;
      puncta[i].position.y += puncta[i].velocity.y;
      puncta[i].velocity.x *= puncta[i].friction;
      puncta[i].velocity.y *= puncta[i].friction;
      if(puncta[i].slave != NULL){
        update_punctum_slave(&puncta[i], puncta[i].slave);
      }
    }
  }
}

void update_punctum_slave(Punctum *master, Punctum *slave){
  slave->direction = (Vector2) convert_angle_to_direct(fmodf(master->angle + slave->angle + slave->rotation, 360.0f));
  slave->position.x += slave->distance * slave->direction.x;
  slave->position.y += slave->distance * slave->direction.y;
  if(slave->slave != NULL){
    update_punctum_slave(slave, slave->slave);
  }
}
