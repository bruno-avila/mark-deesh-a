#include "common.h"

int main(void){
  camera.offset = (Vector2){screen_width / 2.0f, screen_height / 2.0f};
  camera.zoom = 1.0f * screen_width / REAL_WIDTH;
	InitWindow(screen_width, screen_height, "Mark Dash A");

  SetTargetFPS(60);
	while(!WindowShouldClose()){
    update_user_input();
    loop_system();
    draw_system();
	  memset(&user_input, 0, sizeof(user_input));
  }
  CloseWindow();
	return EXIT_SUCCESS;
}
