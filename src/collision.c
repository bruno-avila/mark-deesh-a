#include "common.h"

CollisionBox collision_boxes[COLLISION_BOXES_LIMIT] = {0};

CollisionBox *create_collision_box(Entity *entity, Punctum *punctum){
  for(int i = 0; i < COLLISION_BOXES_LIMIT; i++){
    if(collision_boxes[i].state == 0){
      collision_boxes[i].entity = entity;
      collision_boxes[i].punctum = punctum;
      collision_boxes[i].state = 1;
      return &collision_boxes[i];
    }
  }
  return NULL;
}

void update_collision_boxes(){
  for(int i = 0; i < COLLISION_BOXES_LIMIT; i++){
    if(collision_boxes[i].state != 0){
      collision_boxes[i].rect.x = collision_boxes[i].punctum->position.x - collision_boxes[i].rect.width / 2;
      collision_boxes[i].rect.y = collision_boxes[i].punctum->position.y - collision_boxes[i].rect.height / 2;
    }
  }
}

uint8_t check_aabb_collision(Rectangle a, Rectangle b){
  if(a.x < b.x + b.width &&
  a.x + a.width > b.x &&
  a.y < b.y + b.height &&
  a.y + a.height > b.y){
    return 1;
  }else{
    return 0;
  }
}

void update_collision_boxes_against_solids_v2(){
/*
1-move x axis
2-check collision
3-resolve x
4-move y axis
5-check collision
6-resolve y
*/
  for(uint32_t i = 0; i < COLLISION_BOXES_LIMIT; i++){
    if(collision_boxes[i].state != 0){
      //move x axis
      collision_boxes[i].rect.x = collision_boxes[i].punctum->position.x - collision_boxes[i].rect.width / 2;
      for(uint32_t j = 0; j < SOLIDS_LIMIT; j++){
        if(solids[j].state != 0){
          if(check_aabb_collision(collision_boxes[i].rect, solids[j].rect) == 1){
            //resolve x
            float diff_x = 0;
            if(collision_boxes[i].punctum->position.x < solids[j].rect.x){
              diff_x = collision_boxes[i].rect.x + collision_boxes[i].rect.width - solids[j].rect.x;
            }else if(collision_boxes[i].punctum->position.x > solids[j].rect.x + solids[j].rect.width){
              diff_x = solids[j].rect.x + solids[j].rect.width - collision_boxes[i].rect.x;
            }
            collision_boxes[i].punctum->position.x -= diff_x;
            collision_boxes[i].rect.x = collision_boxes[i].punctum->position.x - collision_boxes[i].rect.width / 2;
          }
        }
      }
      //move y axis
      collision_boxes[i].rect.y = collision_boxes[i].punctum->position.y - collision_boxes[i].rect.height / 2;
      for(uint32_t j = 0; j < SOLIDS_LIMIT; j++){
        if(solids[j].state != 0){
          if(check_aabb_collision(collision_boxes[i].rect, solids[j].rect) == 1){
            //resolve y
            float diff_y = 0;
            if(collision_boxes[i].punctum->position.y < solids[j].rect.y){
              diff_y = collision_boxes[i].rect.y + collision_boxes[i].rect.height - solids[j].rect.y;
            }else if(collision_boxes[i].punctum->position.y > solids[j].rect.y + solids[j].rect.height){
              diff_y = solids[j].rect.y + solids[j].rect.height - collision_boxes[i].rect.y;
            }
            collision_boxes[i].punctum->position.y -= diff_y;
            collision_boxes[i].rect.y = collision_boxes[i].punctum->position.y - collision_boxes[i].rect.height / 2;
          }
        }
      }
    }
  }
}

void update_collision_boxes_against_solids(){
  //can result in problems when colliding with multiple solids at once
  for(int i = 0; i < 256; i++){
    if(collision_boxes[i].state != 0){
      for(int j = 0; j < 256; j++){
        if(solids[j].state != 0){
          if(collision_boxes[i].rect.x < solids[j].rect.x +  solids[j].rect.width &&
          collision_boxes[i].rect.x + collision_boxes[i].rect.width > solids[j].rect.x &&
          collision_boxes[i].rect.y < solids[j].rect.y + solids[j].rect.height &&
          collision_boxes[i].rect.y + collision_boxes[i].rect.height > solids[j].rect.y){
            float diff_x = 0;
            float diff_y = 0;
            solids[j].cor = RED;
            if(collision_boxes[i].punctum->position.x < solids[j].rect.x){
              diff_x = collision_boxes[i].rect.x + collision_boxes[i].rect.width - solids[j].rect.x;
              if(collision_boxes[i].punctum->position.y < solids[j].rect.y){
                diff_y = collision_boxes[i].rect.y + collision_boxes[i].rect.height - solids[j].rect.y;
                if(diff_x > diff_y){
                  collision_boxes[i].punctum->position.y -= diff_y;
                  collision_boxes[i].punctum->velocity.y = 0;
                }else{
                  collision_boxes[i].punctum->position.x -= diff_x;
                  collision_boxes[i].punctum->velocity.x = 0;
                }
              }else if(collision_boxes[i].punctum->position.y > solids[j].rect.y + solids[j].rect.height){
                diff_y = solids[j].rect.y + solids[j].rect.height - collision_boxes[j].rect.y;
                if(diff_x > diff_y){
                  collision_boxes[i].punctum->position.y += diff_y;
                  collision_boxes[i].punctum->velocity.y = 0;
                }else{
                  collision_boxes[i].punctum->position.x -= diff_x;
                  collision_boxes[i].punctum->velocity.x = 0;
                }
              }else{
                collision_boxes[i].punctum->position.x -= diff_x;
                collision_boxes[i].punctum->velocity.x = 0;
              }
            }else if(collision_boxes[i].punctum->position.x > solids[j].rect.x + solids[j].rect.width){
              diff_x = solids[j].rect.x + solids[j].rect.width - collision_boxes[i].rect.x;
              if(collision_boxes[i].punctum->position.y < solids[j].rect.y){
                diff_y = collision_boxes[i].rect.y + collision_boxes[i].rect.height - solids[j].rect.y;
                if(diff_x > diff_y){
                  collision_boxes[i].punctum->position.y -= diff_y;
                  collision_boxes[i].punctum->velocity.y = 0;
                }else{
                  collision_boxes[i].punctum->position.x += diff_x;
                  collision_boxes[i].punctum->velocity.x = 0;
                }
              }else if(collision_boxes[i].punctum->position.y > solids[j].rect.y + solids[j].rect.height){
                diff_y = solids[j].rect.y + solids[j].rect.height - collision_boxes[i].rect.y;
                if(diff_x > diff_y){
                  collision_boxes[i].punctum->position.y += diff_y;
                  collision_boxes[i].punctum->velocity.y = 0;
                }else{
                  collision_boxes[i].punctum->position.x += diff_x;
                  collision_boxes[i].punctum->velocity.x = 0;
                }
              }else{
                collision_boxes[i].punctum->position.x += diff_x;
                collision_boxes[i].punctum->velocity.x = 0;
              }
            }else{
              if(collision_boxes[i].punctum->position.y < solids[j].rect.y){
                diff_y = collision_boxes[i].rect.y + collision_boxes[i].rect.height - solids[j].rect.y;
                collision_boxes[i].punctum->position.y -= diff_y;
                collision_boxes[i].punctum->velocity.y = 0;
              }else if(collision_boxes[i].punctum->position.y > solids[j].rect.y + solids[j].rect.height){
                diff_y = solids[j].rect.y + solids[j].rect.height - collision_boxes[i].rect.y;
                collision_boxes[i].punctum->position.y += diff_y;
                collision_boxes[i].punctum->velocity.y = 0;
              }
            }
          }
          collision_boxes[i].rect = (Rectangle){collision_boxes[i].punctum->position.x - collision_boxes[i].rect.width / 2,
          collision_boxes[i].punctum->position.y - collision_boxes[i].rect.height / 2,
          collision_boxes[i].rect.width,
          collision_boxes[i].rect.height};
        }
      }
    }
  }
}
