#include "common.h"

uint8_t system_state = SYST_MENU;
uint8_t menu_select = 0;

void loop_system(){
  switch(system_state){
  case SYST_MENU:
    main_menu_input();
  break;
  case SYST_GAME:
    loop_game();
  break;
  case SYST_EDITOR:
    loop_editor();
  break;
  }
}

void main_menu_input(){
  uint8_t options = 1;
  if(IsKeyPressed(KEY_UP)){
    if(menu_select == 0){
      menu_select = options;
    }else{
      menu_select--;
    }
  }

  if(IsKeyPressed(KEY_DOWN)){
    if(menu_select == options){
      menu_select = 0;
    }else{
      menu_select++;
    }
  }

  if(IsKeyPressed(KEY_ENTER)){
    switch(menu_select){
    case 0:
      system_state = SYST_GAME;
    break;
    case 1:
      system_state = SYST_EDITOR;
    break;
    }
  }
}

void draw_main_menu(){
  ClearBackground(BLACK);
  Color x1 = GRAY;
  Color x2 = GRAY;
  switch(menu_select){
  case 0:
    x1 = WHITE;
  break;
  case 1:
    x2 = WHITE;
  break;
  }
  DrawText("GAME", 0, 10, 10, x1);
  DrawText("EDITOR", 0, 30, 10, x2);
}

void draw_system(){
  BeginDrawing();
	  ClearBackground(LIGHTGRAY);
		BeginMode2D(camera);
      switch(system_state){
      case SYST_MENU:
        draw_main_menu();
      break;
      case SYST_GAME:
        if(game_state == GMST_RUN){
          draw_game();
        }
      break;
      case SYST_EDITOR:
        if(editor_state == EDST_LOOP){
          draw_editor();
        }
      break;
      }
    EndMode2D();
	EndDrawing();
}
