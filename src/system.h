#ifndef SYSTEM_H
#define SYSTEM_H
#include "common.h"

enum system_state{
  SYST_MENU,
  SYST_GAME,
  SYST_EDITOR,

  SYST_COUNT
};

extern uint8_t system_state;
extern uint8_t menu_select;

void loop_system();
void draw_system();
void main_menu_input();
void draw_main_menu();

#endif
