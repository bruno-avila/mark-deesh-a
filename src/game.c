#include "common.h"
#include "info.h"
#include <raylib.h>

uint8_t game_state = GMST_START;

void loop_game(){
  switch(game_state){
  case GMST_START:
    start_game();
  case GMST_RUN:
    update_player_input(player);
    update_actions();
    update_puncta();
    update_collision_boxes();
    update_collision_boxes_against_solids_v2();
    update_camera_target(player->entity->punctum->position);
  break;
  }
}

void draw_game(){
  DrawRectangleRec(player->entity->collision_box->rect, color_options[0]);
  for(uint16_t i = 0; i < 1024; i++){
    if(solids[i].state != 0){
      DrawRectangleRec(solids[i].rect, solids[i].cor);
    }
  }
}

void start_game(){
  game_state = GMST_RUN;
  game_load_level(0);
}

void game_load_level(uint32_t level_option){
  uint32_t *solids_buffer = NULL;
  uint32_t solids_size;
  char solids_name[256];
  strcpy(solids_name, "data/solid.data");
  solids_buffer = read_file(sizeof(solids_buffer), &solids_size, solids_name);
  parse_solids(solids_buffer, solids_size);

  uint32_t *actors_buffer = NULL;
  uint32_t actors_size;
  char actors_name[256];
  strcpy(actors_name, "data/actor.data");
  actors_buffer = read_file(sizeof(actors_buffer), &actors_size, actors_name);
  parse_actors(actors_buffer, actors_size);

}
