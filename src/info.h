#ifndef INFO_H
#define INFO_H
#include "common.h"

enum actor_type{
  ACTT_PLAYER,

  ACTT_COUNT
};

enum color_option{
  COR_RED,
  COR_RED_ORANGE,
  COR_ORANGE,
  COR_SFT_ORANGE,
  COR_GOLD,
  COR_YELLOW,
  COR_CHLORINE,
  COR_LIME,
  COR_BRG_GREN,
  COR_HARLEQ,
  COR_GREEN,
  COR_SFT_GREEN,
  COR_JADE,
  COR_SPR_GREEN,
  COR_SEA_GREEN,
  COR_AQUA,
  COR_SKY_BLUE,
  COR_AZURE,
  COR_AZUL,
  COR_DRK_AZUL,
  COR_BLUE,
  COR_MARINE,
  COR_INDIGO,
  COR_PURPLE,
  COR_SFT_PURPLE,
  COR_PINK,
  COR_DRK_PINK,
  COR_SHC_PINK,
  COR_RASPBERRY,
  COR_CRIMSON,
  COR_BLACK,
  COR_COAL,
  COR_CARBON,
  COR_GRANITE,
  COR_REY,
  COR_MERCURY,
  COR_SILVER,
  COR_MARBLE,
  COR_WHITE,

  COR_COUNT
};

typedef struct actor_info_t{
  //entity
  uint32_t holding;
  uint32_t state;
  int hp;
  float base_acceleration;
  //punctum
  float friction;
  //collisionbox
  float width;
  float height;
}actor_info_t;

/*
typedef struct color_option_t{
  uint8_t r;
  uint8_t b;
  uint8_t g;
}color_option_t;
*/
extern actor_info_t actor_info[ACTT_COUNT];
extern Color color_options[COR_COUNT];

#endif
