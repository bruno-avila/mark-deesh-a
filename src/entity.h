#ifndef ENTITY_H
#define ENTITY_H
#include "common.h"

typedef struct Entity{
  struct Actor *actor;
  struct Punctum *punctum;
  struct CollisionBox *collision_box;
  uint32_t holding;
  uint32_t state; 
  int hp;
  float base_acceleration;
}Entity;

extern struct Entity entities[];

struct Entity *create_entity(struct Actor *actor);

#endif
