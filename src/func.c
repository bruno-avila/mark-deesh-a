#include "common.h"
#include <stdlib.h>

float convert_direct_to_angle(Vector2 v){
    float angle;
    if(v.y < 0.0f){
        angle = atan2(v.y,v.x) * 180/PI + 360.0f;
    }else{
		angle = atan2(v.y,v.x) * 180/PI;
	}
	return angle;
}

Vector2 convert_angle_to_direct(float angle){
	Vector2 v;
	v.x = cosf(angle * PI/180);
	v.y = sinf(angle * PI/180);
	return v;
}

Vector2 sum_directions(Vector2 v1, Vector2 v2){
	float x = fmodf(convert_direct_to_angle(v1) + convert_direct_to_angle(v2), 360.0f);
	Vector2 v3 = convert_angle_to_direct(x);
	return v3;
}

float sum_angles(float a1, float a2){
	float a3 = fmod(a1 + a2, 360.0f);
	return a3;
}

void *read_file(uint8_t element_size, uint32_t *file_size, char file_name[256]){
  FILE *load_ptr;
  load_ptr = fopen(file_name, "rb");
  fseek(load_ptr, 0, SEEK_END);
  *file_size = ftell(load_ptr);
  fseek(load_ptr, 0, SEEK_SET);

  void *reading_buffer = calloc(*file_size, element_size);
  fread(reading_buffer, element_size, *file_size, load_ptr);
  fclose(load_ptr);

  return reading_buffer;
}
