#include "common.h"
#include "info.h"

Actor actors[256] = {0};
Actor *player;

Actor *create_actor(){
  for(int i = 0; i < 256; i++){
    if(actors[i].state == 0){
      actors[i].action = create_action(&actors[i]);
      actors[i].entity = create_entity(&actors[i]);
      actors[i].state = 1;
      return &actors[i];
    }
  }
  printf("Failed to create Actor!\n");
  return NULL;
}

void parse_actors(uint32_t *actors_buffer, uint32_t actors_size){
  actors_size /= 4;
  for(uint32_t i = 0; i < actors_size; i += 3){
    Actor *act = create_actor();
    float x = (float) actors_buffer[i + 1] * 20;
    float y = (float) actors_buffer[i + 2] * 20;
    set_actor(act, x, y, actors_buffer[i]);
  }
}

void set_actor(Actor *actor, float x, float y, uint32_t actor_type){
  actor->entity->holding = actor_info[actor_type].holding;
  actor->entity->state = actor_info[actor_type].state;
  actor->entity->hp = actor_info[actor_type].hp;
  actor->entity->base_acceleration = actor_info[actor_type].base_acceleration;
  actor->entity->punctum->friction = actor_info[actor_type].friction;
  actor->entity->collision_box->rect.width = actor_info[actor_type].width;
  actor->entity->collision_box->rect.height = actor_info[actor_type].height;
  actor->entity->punctum->position.x = x;
  actor->entity->punctum->position.y = y;
  if(actor_type == ACTT_PLAYER){
    player = actor;
  }
}
