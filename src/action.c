#include "common.h"

Action actions[256] = {0};

Action *create_action(Actor *actor){
  for(int i = 0; i < 256; i++){
    if(actions[i].state == 0){
      actions[i] = (Action){0};
      actions[i].actor = actor;
      actions[i].state = 1;
      return &actions[i];
    }
  }
  return NULL;
}

void update_actions(){
  for(int i = 0; i < 256; i++){
    if(actions[i].state == 1){
      Actor *actor = actions[i].actor;
      switch(actor->state){
      case 1:
        if(actor->action->move_n){
          actor->entity->punctum->velocity.y -= actor->entity->base_acceleration;
        }
        if(actor->action->move_s){
          actor->entity->punctum->velocity.y += actor->entity->base_acceleration;
        }
        if(actor->action->move_e){
          actor->entity->punctum->velocity.x += actor->entity->base_acceleration;
        }
        if(actor->action->move_w){
          actor->entity->punctum->velocity.x -= actor->entity->base_acceleration;
        }
      break;
      default:
        printf("Invalid Actor State\n");
      break;
      }
    }
  }
}
