#include "common.h"
#include "editor.h"
#include "info.h"
#include <raylib.h>

uint8_t layer_type = LATY_SOLID;
uint8_t editor_state = EDST_START;
uint32_t editor_grid_size = EDITOR_GRID_WIDTH * EDITOR_GRID_HEIGHT;
uint8_t *editor_solid_buffer;
uint8_t *editor_actor_buffer;
uint8_t *editor_backg_buffer;
uint8_t *editor_middleg_buffer;
uint8_t *editor_foreg_buffer;
uint8_t *editor_current_buffer;
uint8_t tile_option = COR_WHITE;

void loop_editor(){
  switch(editor_state){
  case EDST_START:
    editor_solid_buffer = calloc(editor_grid_size, sizeof(*editor_solid_buffer));
    memset(editor_solid_buffer, COR_BLACK, editor_grid_size);
    editor_actor_buffer = calloc(editor_grid_size, sizeof(*editor_actor_buffer));
    memset(editor_actor_buffer, COR_BLACK, editor_grid_size);
    editor_backg_buffer = calloc(editor_grid_size, sizeof(*editor_backg_buffer));
    memset(editor_backg_buffer, COR_BLACK, editor_grid_size);
    editor_middleg_buffer = calloc(editor_grid_size, sizeof(*editor_middleg_buffer));
    memset(editor_middleg_buffer, COR_BLACK, editor_grid_size);
    editor_foreg_buffer = calloc(editor_grid_size, sizeof(*editor_foreg_buffer));
    memset(editor_foreg_buffer, COR_BLACK, editor_grid_size);
    editor_current_buffer = editor_solid_buffer;
    editor_state = EDST_LOOP;
    camera.offset = (Vector2){0, 0};
    camera.zoom= 1.0f * screen_height / EDITOR_GRID_HEIGHT;
  case EDST_LOOP:
    update_editor_input();
  break;
  }
}

void draw_editor(){
  //grid
  DrawRectangle(0, 0, EDITOR_GRID_WIDTH, EDITOR_GRID_HEIGHT, color_options[COR_BLACK]);
  uint8_t *drawing_buffer;
  for(uint8_t k = 0; k < LATY_COUNT; k++){
    int i = 0; int j = 0;
    if(k == layer_type){
      for(uint8_t *p = editor_current_buffer; p != editor_current_buffer + editor_grid_size; p++){
        if(*p != COR_BLACK){
          DrawPixel(i, j, color_options[*p]);
        }
        i++;
        if(i == EDITOR_GRID_WIDTH){i = 0; j++;}
      }
    }else{
      switch(k){
      case LATY_SOLID:
        drawing_buffer = editor_solid_buffer;
      break;
      case LATY_ACTOR:
        drawing_buffer = editor_actor_buffer;
      break;
      case LATY_BACKG:
      break;
      case LATY_MIDDLEG:
      break;
      case LATY_FOREG:
      break;
      }
      for(uint8_t *p = drawing_buffer; p != drawing_buffer + editor_grid_size; p++){
        if(*p != COR_BLACK){
          Color cor = color_options[*p];
          cor.a = 40;
          DrawPixel(i, j, cor);
        }
        i++;
        if(i == EDITOR_GRID_WIDTH){i = 0; j++;}
      }
    }
  }


  DrawPixelV(user_input.mouse,color_options[tile_option]);
}

void update_editor_input(){
  if(user_input.mouse.x < EDITOR_GRID_WIDTH && user_input.mouse.x > 0 &&
  user_input.mouse.y < EDITOR_GRID_HEIGHT && user_input.mouse.y > 0){
    if(user_input.mouse_l > INST_UP){
      uint32_t x = user_input.mouse.x;
      uint32_t y = user_input.mouse.y;
      uint8_t *p = editor_current_buffer + EDITOR_GRID_WIDTH * y + x;
      *p = tile_option;
    }
    if(user_input.mouse_r > INST_UP){
      uint32_t x = user_input.mouse.x;
      uint32_t y = user_input.mouse.y;
      uint8_t *p = editor_current_buffer + EDITOR_GRID_WIDTH * y + x;
      *p = COR_BLACK;
    }
  }
  if(user_input.period == INST_PRESS){
    camera.zoom *= 2.0f;
  }
  if(user_input.comma == INST_PRESS){
    camera.zoom *= 0.5f;
  }
  if(user_input.up == INST_PRESS){
    camera.target.y -= screen_height / camera.zoom / 2;
  }
  if(user_input.down == INST_PRESS){
    camera.target.y += screen_height / camera.zoom / 2;
  }
  if(user_input.right == INST_PRESS){
    camera.target.x += screen_width / camera.zoom / 2;
  }
  if(user_input.left == INST_PRESS){
    camera.target.x -= screen_width / camera.zoom / 2;
  }
  if(user_input.c == INST_PRESS){
    change_editing_layer();
  }
  if(user_input.s == INST_PRESS){
    editor_save_file();
  }
  if(user_input.l == INST_PRESS){
    editor_load_file();
  }
  if(user_input.bracket_r == INST_PRESS){
    change_chosen_tile(1);
  }
  if(user_input.bracket_l == INST_PRESS){
    change_chosen_tile(0);
  }
}

void editor_load_file(){
  char file_name[30];
  uint8_t *tmp;
  for(uint8_t i = 0; i < LATY_COUNT; i++){
    memset(file_name, 0, sizeof(file_name));
    switch(i){
    case LATY_SOLID:
      strcpy(file_name, "data/solid.data");
      tmp = editor_solid_buffer;
    break;
    case LATY_ACTOR:
      strcpy(file_name, "data/actor.data");
      tmp = editor_actor_buffer;
    break;
    case LATY_BACKG:
      strcpy(file_name, "data/backg.data");
      tmp = editor_backg_buffer;
    break;
    case LATY_MIDDLEG:
      strcpy(file_name, "data/middleg.data");
      tmp = editor_middleg_buffer;
    break;
    case LATY_FOREG:
      strcpy(file_name, "data/foreg.data");
      tmp = editor_foreg_buffer;
    break;
    }
    editor_read_file(tmp, file_name);
  }
  printf("file loaded.\n");
}

void editor_save_file(){
  char file_name[30];
  uint8_t *tmp;
  for(uint8_t i = 0; i < LATY_COUNT; i++){
    memset(file_name, 0, sizeof(file_name));
    switch(i){
    case LATY_SOLID:
      tmp = editor_solid_buffer;
      strcpy(file_name, "data/solid.data");
    break;
    case LATY_ACTOR:
      tmp = editor_actor_buffer;
      strcpy(file_name, "data/actor.data");
    break;
    case LATY_BACKG:
      tmp = editor_backg_buffer;
      strcpy(file_name, "data/backg.data");
    break;
    case LATY_MIDDLEG:
      tmp = editor_middleg_buffer;
      strcpy(file_name, "data/middleg.data");
    break;
    case LATY_FOREG:
      tmp = editor_foreg_buffer;
      strcpy(file_name, "data/foreg.data");
    break;
    }
    editor_write_file(tmp, file_name);
  }
  printf("file saved\n");
}

void editor_read_file(uint8_t *reading_buffer, char file_name[30]){
  FILE *load_ptr;
  uint32_t *file_buffer;
  load_ptr = fopen(file_name, "rb");

  fseek(load_ptr, 0, SEEK_END);
  uint32_t file_size = ftell(load_ptr);
  fseek(load_ptr, 0, SEEK_SET);
  
  file_buffer = calloc(file_size, 4);
  fread(file_buffer, 4, file_size, load_ptr);
  uint8_t parameters = 3;
  
  memset(reading_buffer, COR_BLACK, editor_grid_size);
  uint32_t x,y;
  for(uint32_t i = 0; i < file_size; i += parameters){
    x = file_buffer[i + 1];
    y = file_buffer[i + 2];
    reading_buffer[x + (y * EDITOR_GRID_WIDTH)] = file_buffer[i];  
  }
  fclose(load_ptr);
}

void editor_write_file(uint8_t *writing_buffer, char file_name[30]){
  uint32_t element_count = 0;
  uint8_t parameters = 3; //cor, x, y:
  for(uint32_t i = 0; i < editor_grid_size; i++){
    if(writing_buffer[i] != COR_BLACK){
      element_count++;
    }
  }
  uint32_t file_size = parameters * element_count;
  uint32_t *file_buffer;
  uint32_t j = 0;
  uint32_t x = 0;
  uint32_t y = 0;
  file_buffer = calloc(file_size, 4);
  for(uint32_t i = 0; i < editor_grid_size; i++){
    if(writing_buffer[i] != COR_BLACK){
      file_buffer[j] = writing_buffer[i];
      file_buffer[j + 1] = x;
      file_buffer[j + 2] = y;
      j += parameters;
    }
    x++;
    if(x == EDITOR_GRID_WIDTH){x = 0; y++;}
  }
  FILE *write_ptr;
  write_ptr = fopen(file_name, "wb");
  fwrite(file_buffer, 4, file_size, write_ptr);
  fclose(write_ptr);
}

void change_chosen_tile(uint8_t val){
  if(val == 1){
    if(tile_option + 1 == COR_COUNT){
      tile_option = 0;
    }else{
      tile_option++;
    }
  }else{
    if(tile_option == 0){
      tile_option = COR_COUNT - 1;
    }else{
      tile_option--;
    }
  }  
  printf("%i\n",tile_option);
}

void change_editing_layer(){
  if(layer_type + 1 == LATY_COUNT){
    layer_type = 0;
  }else{
    layer_type++;
  }
  
  switch(layer_type){
  case LATY_SOLID:
    editor_current_buffer = editor_solid_buffer;
    printf("Layer: Solid\n");
  break;
  case LATY_ACTOR:
    printf("Layer: Actor\n");
    editor_current_buffer = editor_actor_buffer;
  break;
  case LATY_BACKG:
    printf("Layer: Background\n");
    editor_current_buffer = editor_backg_buffer;
  break;
  case LATY_MIDDLEG:
    printf("Layer: Middleground\n");
    editor_current_buffer = editor_middleg_buffer;
  break;
  case LATY_FOREG:
    printf("Layer: Foreground\n");
    editor_current_buffer = editor_foreg_buffer;
  break;
  }
}
