#ifndef GAME_H
#define GAME_H
#include "common.h"

enum game_state{
  GMST_START,
  GMST_RUN,
  GMST_PAUSE,

  GMST_COUNT
};

extern uint8_t game_state;

void loop_game();
void draw_game();
void start_game();
void game_load_level(uint32_t level_option);
void game_load_solids();
#endif
