#include "common.h"

Entity entities[256] = {0};

Entity *create_entity(Actor *actor){
  for(int i = 0; i < 256; i++){
    if(entities[i].state == 0){
      entities[i].actor = actor;
      entities[i].punctum = create_punctum(NULL);
      entities[i].collision_box = create_collision_box(&entities[i], entities[i].punctum);
      entities[i].state = 1;
      return &entities[i];
    }
  }
  printf("Failed to create Entity!\n");
  return NULL;
}


