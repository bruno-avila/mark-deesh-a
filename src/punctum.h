#ifndef PUNCTUM_H
#define PUNCTUM_H
#include "common.h"

typedef struct Punctum{
  Vector2 position;
  Vector2 direction;
  Vector2 velocity;
  struct Punctum *master;
  struct Punctum *slave;
  float friction;
  float angle;
  float rotation;
  float distance;
  uint8_t state;
}Punctum;

extern struct Punctum puncta[];

struct Punctum *create_punctum(struct Punctum *punctum);
void delete_punctum(struct Punctum *p);
void update_puncta();
void update_punctum_slave(struct Punctum *master, struct Punctum *slave);

#endif
