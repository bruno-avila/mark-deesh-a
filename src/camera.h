#ifndef CAMERA_H
#define CAMERA_H
#include "common.h"

/*do not change these*/
#define REAL_WIDTH 960
#define REAL_HEIGHT 540

extern uint16_t screen_width;
extern uint16_t screen_height;
extern Camera2D camera;

void update_camera_target(Vector2 position);

#endif
