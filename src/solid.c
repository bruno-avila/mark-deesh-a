#include "common.h"

Solid solids[SOLIDS_LIMIT] = {0};

Solid *create_solid(){
  for(uint16_t i = 0; i < SOLIDS_LIMIT; i++){
    if(solids[i].state == 0){
      solids[i].state = 1;
      return &solids[i];
    }
  }
  printf("Failed to create Solid!\n");
  return NULL;
}

void set_solid(Solid *solid, float x, float y, float w, float h, Color cor){
  solid->rect.x = x;
  solid->rect.y = y;
  solid->rect.width = w;
  solid->rect.height = h;
  solid->cor = cor;
}

void parse_solids(uint32_t *solids_buffer, uint32_t solids_size){
  solids_size /= 4;
  for(uint32_t i = 0; i < solids_size; i += 3){
    Solid *sol = create_solid();
    float x = (float) solids_buffer[i + 1]*20;
    float y = (float) solids_buffer[i + 2]*20;
    Color cor = color_options[solids_buffer[i]];
    set_solid(sol, x, y, 20.0f, 20.0f, cor);
  }
}
