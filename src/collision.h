#ifndef COLLISION_H
#define COLLISION_H
#include "common.h"

#define COLLISION_BOXES_LIMIT 256

typedef struct CollisionBox{
  struct Entity *entity;
  struct Punctum *punctum;
  Rectangle rect;
  uint32_t state;
}CollisionBox;

extern struct CollisionBox collisions_boxes[];

struct CollisionBox *create_collision_box(struct Entity *entity, struct Punctum *punctum);
uint8_t check_aabb_collision(Rectangle a, Rectangle b);
void update_collision_boxes();
void update_collision_boxes_against_solids();
void update_collision_boxes_against_solids_v2();

#endif
