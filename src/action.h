#ifndef ACTION_H
#define ACTION_H
#include "common.h"

typedef struct Action{
  struct Actor *actor;
  uint8_t move_n;//north
  uint8_t move_s;//south
  uint8_t move_e;//east
  uint8_t move_w;//west
  uint8_t action_0;//left mouse
  uint8_t action_1;//right mouse
  
  uint8_t state;
}Action;

extern struct Action actions[];

struct Action *create_action(struct Actor *actor);
void update_actions();

#endif
