#ifndef INPUT_H
#define INPUT_H
#include "common.h"

/*
Raylib only supports the US keyboard by default
br-abnt2 will require redefining
*/

enum input_state{
  INST_UP,
  INST_PRESS,
  INST_DOWN,

  INST_COUNT
}; 

typedef struct Input{
  Vector2 mouse;
  uint8_t mouse_r;
  uint8_t mouse_l;
  uint8_t comma; //   ,  <
  uint8_t period; //  .  >
  uint8_t up;
  uint8_t down;
  uint8_t right;
  uint8_t left;
  uint8_t c;
  uint8_t l;
  uint8_t s;
  uint8_t bracket_r;
  uint8_t bracket_l;
  uint8_t enter;

  uint8_t state;
}Input;

extern struct Input user_input;

void update_user_input();
void update_player_input(Actor *player);

#endif
