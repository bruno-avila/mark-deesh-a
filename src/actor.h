#ifndef ACTOR_H
#define ACTOR_H
#include "common.h"

typedef struct Actor{
  struct Action *action;
  //struct Animation *animation;
  struct Entity *entity;
  uint32_t state;
}Actor;

extern struct Actor actors[];
extern struct Actor *player;

struct Actor *create_actor();
void parse_actors(uint32_t *actors_buffer, uint32_t actors_size);
void set_actor(struct Actor *actor, float x, float y, uint32_t actor_type);

#endif
