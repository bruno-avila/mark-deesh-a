#ifndef FUNC_H
#define FUNC_H
#include "common.h"

float convert_direct_to_angle(Vector2 v);
Vector2 convert_angle_to_direct(float angle);
Vector2 sum_directions(Vector2 v1, Vector2 v2);
float sum_angles(float a1, float a2);
void *read_file(uint8_t element_size, uint32_t *file_size, char file_name[256]);

#endif
