#include "common.h"

uint16_t screen_width = 960;
uint16_t screen_height = 540;

Camera2D camera = {0};

void update_camera_target(Vector2 position){
  camera.target = position;
}
