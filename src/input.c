#include "common.h"

Input user_input = {0};

void update_user_input(){
  user_input.mouse = GetScreenToWorld2D(GetMousePosition(), camera);
  if(IsMouseButtonPressed(MOUSE_RIGHT_BUTTON)){
    user_input.mouse_r = INST_PRESS;
  }else if(IsMouseButtonDown(MOUSE_RIGHT_BUTTON)){
    user_input.mouse_r = INST_DOWN;
  }

  if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
    user_input.mouse_l = INST_PRESS;
  }else if(IsMouseButtonDown(MOUSE_LEFT_BUTTON)){
    user_input.mouse_l = INST_DOWN;
  }

  if(IsKeyPressed(KEY_COMMA)){
    user_input.comma = INST_PRESS;
  }else if(IsKeyDown(KEY_COMMA)){
    user_input.comma = INST_DOWN;
  }

  if(IsKeyPressed(KEY_PERIOD)){
    user_input.period = INST_PRESS;
  }else if(IsKeyDown(KEY_PERIOD)){
    user_input.period = INST_DOWN;
  }

  if(IsKeyPressed(KEY_UP)){
    user_input.up = INST_PRESS;
  }else if(IsKeyDown(KEY_UP)){
    user_input.up = INST_DOWN;
  }

  if(IsKeyPressed(KEY_DOWN)){
    user_input.down = INST_PRESS;
  }else if(IsKeyDown(KEY_DOWN)){
    user_input.down = INST_DOWN;
  }

  if(IsKeyPressed(KEY_RIGHT)){
    user_input.right = INST_PRESS;
  }else if(IsKeyDown(KEY_RIGHT)){
    user_input.right = INST_DOWN;
  }

  if(IsKeyPressed(KEY_LEFT)){
    user_input.left = INST_PRESS;
  }else if(IsKeyDown(KEY_LEFT)){
    user_input.left = INST_DOWN;
  }

  if(IsKeyPressed(KEY_C)){
    user_input.c = INST_PRESS;
  }else if(IsKeyDown(KEY_C)){
    user_input.c = INST_DOWN;
  }

  if(IsKeyPressed(KEY_L)){
    user_input.l = INST_PRESS;
  }else if(IsKeyDown(KEY_L)){
    user_input.l = INST_DOWN;
  }

  if(IsKeyPressed(KEY_S)){
    user_input.s = INST_PRESS;
  }else if(IsKeyDown(KEY_S)){
    user_input.s = INST_DOWN;
  }

  if(IsKeyPressed(KEY_RIGHT_BRACKET)){
    user_input.bracket_r = INST_PRESS;
  }else if(IsKeyDown(KEY_RIGHT_BRACKET)){
    user_input.bracket_r = INST_DOWN;
  }

  if(IsKeyPressed(KEY_LEFT_BRACKET)){
    user_input.bracket_l = INST_PRESS;
  }else if(IsKeyDown(KEY_LEFT_BRACKET)){
    user_input.bracket_l = INST_DOWN;
  }

  if(IsKeyPressed(KEY_ENTER)){
    user_input.enter = INST_PRESS;
  }else if(IsKeyDown(KEY_ENTER)){
    user_input.enter = INST_DOWN;
  }
}

void update_player_input(Actor *player){
  if(IsKeyDown(KEY_UP)){
    player->action->move_n = 1;
  }else{
    player->action->move_n = 0;
  }
  if(IsKeyDown(KEY_DOWN)){
    player->action->move_s = 1;
  }else{
    player->action->move_s = 0;
  }
  if(IsKeyDown(KEY_RIGHT)){
    player->action->move_e = 1;
  }else{
    player->action->move_e = 0;
  }
  if(IsKeyDown(KEY_LEFT)){
    player->action->move_w = 1;
  }else{
    player->action->move_w = 0;
  }
}
